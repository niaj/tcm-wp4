<?php
$owner       = false;
$page_author = get_user_by( "slug", get_query_var( "author_name" ) );
$uid         = get_current_user_id();
if ( $uid == $page_author->data->ID ) {
	$owner = true;
//	echo "You're Owner";
} else {
//	echo "You're visitor";
}
$page = get_query_var( "paged" );
if ( $page <= 1 ) {
	?>

	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title></title>
		<style>
			.main {
				padding-top: 50px;
			}
		</style>
		<?php wp_head(); ?>
	</head>
	<body <?php body_class() ?>>
	<div class="container main">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<?php if ( $owner ) { ?>
					<form action="" enctype="multipart/form-data">
						<textarea id="status" class="form-control" rows="10"></textarea>
						<br/><input type="file" id="image">
						<br/>
						<button id="poststatus" class="btn">Post Status</button>
						<input type="hidden" id="nonce" value="<?php echo wp_create_nonce( 'status' ); ?>">
					</form>
				<?php } ?>

				<div id="statuses">
					<?php
					while ( have_posts() ) {
						the_post();
						echo "<h1>";
						the_title();
						echo "</h1>";

						$media = get_attached_media( "image" );
						if ( $media ) {
							$attachment    = array_pop( $media );
							$attachment_id = $attachment->ID;
							$image         = wp_get_attachment_image_src( $attachment_id, "medium" );
							echo "<img src='{$image[0]}'/>";
						}
					}
					?>
				</div>

				<button id="loadmore" class="btn"> Load More </button>

			</div>
		</div>
	</div>
	<?php wp_footer(); ?>
	</body>
	</html>
	<?php
} else {
	while ( have_posts() ) {
		the_post();
		echo "<h1>";
		the_title();
		echo "</h1>";

		$media = get_attached_media( "image" );
		if ( $media ) {
			$attachment    = array_pop( $media );
			$attachment_id = $attachment->ID;
			$image         = wp_get_attachment_image_src( $attachment_id, "medium" );
			echo "<img src='{$image[0]}'/>";
		}
	}
}

?>