<?php
add_action("init","redirect_user");
function redirect_user(){
	if(is_admin() && !defined("DOING_AJAX")){
		if(!current_user_can("manage_options")){
			$uid = get_current_user_id();
			$page = get_author_posts_url($uid);
			wp_redirect($page);
		}
	}
}

//add_action("init","author_base_change");
function author_base_change(){
	global $wp_rewrite;
	$wp_rewrite->author_base="users";
	$wp_rewrite->flush_rules();
}

add_action("wp_ajax_status","post_status");
function post_status(){
	$status = $_POST['s'];
	$nonce = $_POST['n'];
	if(wp_verify_nonce($nonce,"status")){
		$postargs = array(
			"post_title"=>$status,
			"post_content"=>$status,
			"post_status"=>"publish",
			"post_author"=>get_current_user_id()
		);
		$post_id = wp_insert_post($postargs);

		if(isset($_FILES['image'])){
			$attachment_id = media_handle_upload("image",$post_id);
			$image = wp_get_attachment_image_src($attachment_id,"medium");
			echo "<h1>".$status."</h1>";
			echo "<img src='{$image[0]}'/>";
		}else{
			echo "<h1>".$status."</h1>";
		}


	}
	wp_die();
}

















