<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
	<style>
		.main{
			padding-top: 50px;
		}
	</style>
	<?php wp_head(); ?>
</head>
<body <?php body_class() ?>>
<div class="container main">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">


			<div id="statuses">
				<?php
				while(have_posts()){
					the_post();
					echo "<h1>";
					the_title();
					echo "</h1>";
					the_author();
					echo "<br/>";

					$media  = get_attached_media("image");
					if($media){
						$attachment = array_pop($media);
						$attachment_id = $attachment->ID;
						$image = wp_get_attachment_image_src($attachment_id,"medium");
						echo "<img src='{$image[0]}'/>";
					}
				}
				?>
			</div>

		</div>
	</div>
</div>
<?php wp_footer(); ?>
</body>
</html>